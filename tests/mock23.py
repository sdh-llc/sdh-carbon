"""
Mock wrapper for compatibility layer
"""
try:
    from unittest.mock import patch, MagicMock, PropertyMock
except ImportError:
    from mock import patch, MagicMock, PropertyMock
