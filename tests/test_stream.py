from __future__ import unicode_literals

import unittest

from tests.mock23 import patch, MagicMock

from sdh.carbon import stream
from sdh.carbon.conf import settings


class TestStream(unittest.TestCase):

    def test_row_format(self):
        stamp = 1529578889.878235
        rc = stream.row_format('test-host', 'm1', 123, stamp)
        self.assertEqual(rc, 'test-host.m1 123 1529578889')

    @patch('sdh.carbon.stream.send_buff')
    @patch('sdh.carbon.stream.time', MagicMock(return_value=1529578889))
    @patch('sdh.carbon.stream.get_hostname', MagicMock(return_value='test-host'))
    def test_send(self, send_buff):
        stream.send('test', 1)
        send_buff.assert_called_once_with(settings.CARBON_PORT, ['test-host.test 1 1529578889'])

    @patch('sdh.carbon.stream.send_buff')
    @patch('sdh.carbon.stream.time', MagicMock(return_value=1529578889))
    @patch('sdh.carbon.stream.get_hostname', MagicMock(return_value='test-host'))
    def test_send_time_1(self, send_buff):
        stream.send_time('test', 2)
        send_buff.assert_called_once_with(settings.CARBON_PORT, ['test-host.test.time 2 1529578889'])

    @patch('sdh.carbon.stream.send_buff')
    @patch('sdh.carbon.conf.ConfigManager.DEFAULT', {'CARBON_SEND_COUNT': True})
    @patch('sdh.carbon.stream.time', MagicMock(return_value=1529578889))
    @patch('sdh.carbon.stream.get_hostname', MagicMock(return_value='test-host'))
    def test_send_time_2(self, send_buff):
        stream.send_time('test', 3)
        send_buff.assert_called_once_with(settings.CARBON_PORT, ['test-host.test.time 3 1529578889',
                                                                 'test-host.test.count 1 1529578889'])

    @patch('sdh.carbon.stream.send_buff')
    @patch('sdh.carbon.conf.ConfigManager.DEFAULT', {'CARBON_SEND_ALL': True})
    @patch('sdh.carbon.stream.time', MagicMock(return_value=1529578889))
    @patch('sdh.carbon.stream.get_hostname', MagicMock(return_value='test-host'))
    def test_send_time_3(self, send_buff):
        stream.send_time('test', 4)
        send_buff.assert_called_once_with(settings.CARBON_PORT, ['test-host.test.time 4 1529578889',
                                                                 'all.test.time 4 1529578889'])
